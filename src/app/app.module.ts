//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from './shared/ShareModules';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule, OAuthService } from 'angular-oauth2-oidc';

//components
import { HomeComponent } from './views/home/home.component';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './shared/containers/full-layout/full-layout.component';
import { LayoutComponent } from './shared/containers/layout/layout.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { MenuComponent } from './shared/components/menu/menu.component';
import { ErrorPageComponent } from './views/error-page/error-page.component';

//services
import { AuthGuardService } from './shared/services/auth/auth-guard.service';
import { AuthService } from './shared/services/auth/auth.service';
import { WebStorageService } from './shared/services/common/webStorage.service';
import { NgxUiLoaderModule } from 'ngx-ui-loader';

import { environment } from 'src/environments/environment';
const ALLOWED_URL: string = environment.apiGatewayURL;

@NgModule({
  imports: [
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [ALLOWED_URL],
        sendAccessToken: true
      }
    }),
    HttpClientModule,
    SharedModule,
    NgbModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-center',
      closeButton: true,
      preventDuplicates: true,
      enableHtml: true
    }),
    NgxUiLoaderModule
  ],
  providers: [
    AuthGuardService,
    OAuthService,
    AuthService,
    WebStorageService
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    LayoutComponent,
    HeaderComponent,
    MenuComponent,
    ErrorPageComponent,
    HomeComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
