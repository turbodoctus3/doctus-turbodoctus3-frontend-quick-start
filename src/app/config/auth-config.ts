import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

export const authConfig: AuthConfig = {

    // Url of the Identity Provider
    issuer: environment.auth.issuer,

    requireHttps : false,

    showDebugInformation : true,

    //// Login Url of the Identity Provider
    loginUrl: environment.auth.loginUrl,

    //// Login Url of the Identity Provider
    logoutUrl: environment.auth.logoutUrl,

    // URL of the SPA to redirect the user to after login
    //redirectUri: "http://localhost:4200",
    redirectUri: environment.auth.redirectUri,

    // The SPA's id. The SPA is registerd with this id at the auth-server
    clientId: 'AngularAdmin',

    // set the scope for the permissions the client should request
    // The first three are defined by OIDC. Also provide user sepecific
    scope: 'openid profile email roles api1',

    responseType : "id_token token",

    jwks: {
        keys: [
          {
            kty: environment.auth.key.kty,
            use:  environment.auth.key.use,
            kid:  environment.auth.key.kid,
            e:  environment.auth.key.e,
            n:  environment.auth.key.n,
            alg:  environment.auth.key.alg
          }
        ]
      }
}

