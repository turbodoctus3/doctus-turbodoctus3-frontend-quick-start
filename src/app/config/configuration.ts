export const APIENDPOINT = {
    /* Example Apis */
    /*
    schema: {
        entity1:{
            getAll: 'api/entity1/getAll',
            create: 'api/entity1/create/',
            edit: 'api/entity1/edit',
            delete: 'api/entity1/delete',
            detail: 'api/entity1/details/',
            getMasterFilter: 'api/entity1/getMasterFilter',
            exportListToExcel: 'api/entity1/generateExcel/',
            uploadFiles: 'api/entity1/uploadMultipleFiles',
        },
        entity2:{
            getAll: 'api/entity2/getAll',
            create: 'api/entity2/create/',
            edit: 'api/entity2/edit',
            delete: 'api/entity2/delete',
            detail: 'api/entity2/details/',
            getMasterFilter: 'api/entity2/getMasterFilter',
            exportListToExcel: 'api/entity2/generateExcel/',
            uploadFiles: 'api/entity2/uploadMultipleFiles',
        }
    }
    */
};

export const WEB_STORAGE_KEYS = {
    ID_TOKEN : "id_token_claims_obj"
  }
