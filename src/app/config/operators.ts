export const Operators = {
    equals: 'Equals',
    contains: 'Contains',
    greterThan: 'GreaterThan',
    greaterThanOrEqual: 'GreaterThanOrEqual',
    lessThan: 'LessThan',
    lessThanOrEqualTo: 'LessThanOrEqualTo'
}
