export class UserModel {
    public userId: number;
    public role : string;
    public email : string;
    public given_name : string;
}
