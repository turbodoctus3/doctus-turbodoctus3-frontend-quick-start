import { PaginationModel } from './pagination.model';

export class FileGeneratorModel{
    public model: any;
    public view: string;
    public listName: string;
    public paramFilters: PaginationModel;
}