import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private oauthService : OAuthService){
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return (this.oauthService.hasValidAccessToken() && this.oauthService.hasValidIdToken());
    }
}