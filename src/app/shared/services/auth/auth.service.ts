import { Injectable } from '@angular/core';
import 'rxjs/add/operator/mergeMap';
import { WebStorageService } from '../common/webStorage.service';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../../models/user/user.model';
import { navigation } from 'src/app/_nav';
import { WEB_STORAGE_KEYS } from 'src/app/config/configuration';

@Injectable()
export class AuthService  {

    constructor(
        protected _httpClient: HttpClient,
        protected _webStorageService: WebStorageService){
        }

    public loggedInUser(): UserModel {
        return JSON.parse(this._webStorageService.getSessionStorage(WEB_STORAGE_KEYS.ID_TOKEN));
    };

    public getNavigation(){
        return navigation;
    }


}
