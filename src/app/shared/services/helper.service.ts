import { Injectable } from "@angular/core";
import { DateModel } from "../models/common/date.model";
import { Message } from '@angular/compiler/src/i18n/i18n_ast';


@Injectable()
export class HelperService {

    constructor() { }

    /**
     * Metodo que recibe una lista de errores juntos en un string y la convierte a un mensaje
     * mas amigable al usuario final
     * @param errorMessages 
     */
    formatToErrorMessage(errorMessages: Array<string>): string {
        let message = "<ul>";
        for (let index = 0; index < errorMessages.length; index++) {
            const error = errorMessages[index];
            console.log(error);
            message = message + "<li>" + error + "</li>";
        }
        message = message + "</ul>"
        return message;
    }
}