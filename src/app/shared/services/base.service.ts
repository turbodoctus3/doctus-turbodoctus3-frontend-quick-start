import { Observable, throwError } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { environment } from "../../../environments/environment";
import { ResponseModel } from "../models/common/response.model";
import { HeadersConfig } from "../../config/headers";
import { HttpClient } from "@angular/common/http";
import { PaginationModel } from "../models/common/pagination.model";
import { PaginatioResponseModel } from "../models/common/pagination-response.model";
import { DeleteModel } from "../models/common/delete.model";
import { FileGeneratorModel } from "../models/common/file-generator.model";
import { FileModel } from "../models/common/file.model";
import { ResponseFilterModel } from "../models/common/response-filter.model";
import { retry, catchError } from "rxjs/operators";
import { GlobalConstantsModel } from "../common/constants/global.constants";

export abstract class BaseService<TModel> {
  public headersConfig: HeadersConfig = new HeadersConfig();

  constructor(
    protected _httpClient: HttpClient,
    protected _apiRoot: string = environment.apiGatewayURL,
    protected _serviceEndpoints
  ) {}

  handleError(ex): Observable<ResponseModel<any>> {
    const errorModel: ResponseModel<any> = new ResponseModel<any>();
    errorModel.header = ex.error.header;
    errorModel.data = ex.error.data;
    return throwError(errorModel);
  }

  mapObjectToResponseModel(entity: any): ResponseModel<any> {
    let responseModel: ResponseModel<any> = new ResponseModel<any>();
    responseModel.header = entity.header;
    responseModel.data = entity.data;
    return responseModel;
  }

  getAll(): Observable<ResponseModel<TModel[]>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.getAll}`;
    return this._httpClient
      .get(apiURL)
      .map((resp: ResponseModel<any>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }

  getAllPaged(
    object: PaginationModel
  ): Observable<ResponseModel<PaginatioResponseModel<TModel[]>>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.getAll}`;
    return this._httpClient
      .post(apiURL, object, { headers: this.headersConfig.httpHeaders })
      .map((resp: ResponseModel<PaginatioResponseModel<TModel[]>>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }

  getDetails(query: string): Observable<ResponseModel<TModel>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.detail}${query}`;
    return this._httpClient
      .get(apiURL)
      .map((resp: ResponseModel<TModel>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }

  create(object: TModel): Observable<ResponseModel<TModel>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.create}`;
    return this._httpClient
      .post(apiURL, object, { headers: this.headersConfig.httpHeaders })
      .map((resp: ResponseModel<TModel>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }

  update(object: TModel): Observable<ResponseModel<TModel>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.edit}`;
    return this._httpClient
      .post(apiURL, object, { headers: this.headersConfig.httpHeaders })
      .map((resp: ResponseModel<TModel>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }

  delete(object: DeleteModel): Observable<ResponseModel<TModel>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.delete}`;
    return this._httpClient
      .post(apiURL, object, { headers: this.headersConfig.httpHeaders })
      .map((resp: ResponseModel<TModel>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }

  exportExcel(
    excelGeneratorModel: FileGeneratorModel
  ): Observable<ResponseModel<any>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.exportListToExcel}`;
    return this._httpClient
      .post(apiURL, excelGeneratorModel, {
        headers: this.headersConfig.httpHeaders
      })
      .map((resp: ResponseModel<any>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }

  saveMultipleFiles(
    fileRequestModel: Array<FileModel>
  ): Observable<ResponseModel<string>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.uploadFiles}`;
    return this._httpClient
      .post(apiURL, fileRequestModel, {
        headers: this.headersConfig.httpHeaders
      })
      .map((resp: ResponseModel<string>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }

  getMasterFilter(): Observable<ResponseModel<ResponseFilterModel>> {
    const apiURL = `${this._apiRoot}${this._serviceEndpoints.getMasterFilter}`;
    return this._httpClient
      .get(apiURL)
      .map((resp: ResponseModel<ResponseFilterModel>) => {
        return this.mapObjectToResponseModel(resp);
      })
      .pipe(
        retry(GlobalConstantsModel.SERVICES_RETRY_NUMBER),
        catchError(this.handleError)
      );
  }
}
