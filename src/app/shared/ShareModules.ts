// Modulos
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { PaginationModule } from 'ngx-bootstrap/pagination';

// Componentes
import { AppDeleteRegisterComponent } from './components/app-delete-register/app-delete-register.component';
import { AppFieldRequitedComponent } from './components/app-field-required/app-field-required.component';
import { FiltroCampoComponent } from './components/app-filter';
import { FiltroComponent } from './components/app-filter/filtro';
import { BreadCrumbsComponent } from './components/bread-crumbs/bread-crumbs.component';
import { AppFileUploadComponent, AppLoaderComponent } from './components';
import { NgxUiLoaderModule } from 'ngx-ui-loader';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule.forRoot(),
    PaginationModule.forRoot(),
    NgxUiLoaderModule
  ],
  declarations: [
    AppFileUploadComponent,
    AppDeleteRegisterComponent,
    AppFieldRequitedComponent,
    FiltroCampoComponent,
    FiltroComponent,
    BreadCrumbsComponent,
    AppLoaderComponent
  ],
  exports: [
    AppFileUploadComponent,
    AppDeleteRegisterComponent,
    AppFieldRequitedComponent,
    FiltroCampoComponent,
    FiltroComponent,
    BreadCrumbsComponent,
    AppLoaderComponent
  ]
})

export class SharedModule { }
