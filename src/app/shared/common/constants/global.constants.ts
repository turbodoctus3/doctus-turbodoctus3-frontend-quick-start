export const GlobalConstantsModel = {
    REQUIRED: "El campo es requerido",
    INTERNAL_SERVER_ERROR: "El servidor no se encuentra disponible en el momento. Por favor comuniquese con el administrador.",
    SUCCESFULL_CREATE: "El registro se creo exitosamente",
    SELECT_VALID_VALUE: "Seleccione un item valido por favor",
    SERVICES_RETRY_NUMBER: 0,
}
