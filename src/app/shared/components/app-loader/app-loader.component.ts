import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-loader',
    templateUrl: './app-loader.component.html'
})
export class AppLoaderComponent {
    
    @Input() isMasterLoader: boolean;

    public AppLoaderComponent()
    {
        this.isMasterLoader = false; 
    }

}