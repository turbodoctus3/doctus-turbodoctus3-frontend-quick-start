import { Component, Input } from '@angular/core';

// Model
import { FilterParamsModel } from '../../../models/common/filter-params.model';
import { PaginationModel } from '../../../models/common/pagination.model';
import { ResponseFilterModel } from '../../../models/common/response-filter.model';

// Service

@Component({
    selector: 'filtro-component',
    templateUrl: 'filtro.component.html'
})
export class FiltroComponent {
    @Input() paginationModel: PaginationModel;
    @Input() responseFilterModel: ResponseFilterModel;
    @Input() search: Function;
    showFilters: boolean;

    filterParamsModel: FilterParamsModel = new FilterParamsModel();
    camposFiltro: Array<FilterParamsModel> = new Array<FilterParamsModel>();

    iconAddFilter : String ;

    constructor(
    ) {

    };

    ngOnInit() {
        this.addFilter();
        this.iconAddFilter = "assets/images/icon-add-condition.png";
    };

    addFilter() {
        let nf: FilterParamsModel = new FilterParamsModel();
        this.camposFiltro.push(nf);
    }

    removeFilter(element: any) {
        this.camposFiltro.splice(element, 1);
    }

    getAllFilter() {
        this.search(this.camposFiltro);
    }

    openFilter(){
        this.showFilters = !this.showFilters;
    }
}
