import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../models/user/user.model';
import { AuthService } from '../../services/auth/auth.service';
import { RolEnum } from '../../enums/rol-enum.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  
  public user: UserModel ;
  public rol ;
  public navigation;

  logoDoctus: string;
  iconOptions: string;

  constructor(private authService : AuthService) { 

      //la implementacion de la parte de autenticacion esta inicialmente comentada
      //El desarrolador decide de que forma va desarrollar el inicio de session en la aplicacion 
      // this.user = authService.loggedInUser();
      // this.rol = this.user.role;
      this.rol = RolEnum.administrador;
      this.navigation = authService.getNavigation();

      this.logoDoctus = "assets/images/symbol-doctus.png";
      this.iconOptions = "assets/images/icon-settings.png";
      
  }

  ngOnInit() {
  }

  public AllowUser(item) {
    let show: boolean = false;
    for (let rol of item.roles) {
      if (rol == this.rol) {
        show = true;
      }
    }

    return show;
  }

}
