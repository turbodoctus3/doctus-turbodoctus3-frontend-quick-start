import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Breadcrumb } from './models/breadcrumb.model';
import 'rxjs/add/operator/filter';

@Component({
    selector: 'app-bread-crumbs',
    templateUrl: './bread-crumbs.component.html',
    styleUrls: ['./bread-crumbs.component.scss']
})
export class BreadCrumbsComponent implements OnInit {

    @Input() breadcrumbs: Array<Breadcrumb> = new Array<Breadcrumb>();
    @Input() buildFromRoute: boolean = true;

    constructor(private router: Router, private route: ActivatedRoute) {
        if (this.buildFromRoute) {
            this.loadBreadcrumbFromRoute();
        }
    }

    ngOnInit() {
    }

    redirect(url) {
        this.router.navigate([url]);
    }

    loadBreadcrumbFromRoute() {
        this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event) => {
            this.breadcrumbs = [];
            let currentRoute = this.route.root,
                url = '';
            do {
                const childrenRoutes = currentRoute.children;
                currentRoute = null;

                childrenRoutes.forEach(route => {
                    if (route.outlet === 'primary') {
                        const routeSnapshot = route.snapshot;
                        url += '/' + routeSnapshot.url.map(segment => segment.path).join('/');

                        if (route.snapshot.data && route.snapshot.data.title) {
                            this.breadcrumbs.push(new Breadcrumb(route.snapshot.data.title, url));
                        }
                        currentRoute = route;
                    }
                });
            } while (currentRoute);
        });
    }
}
