import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  iconSearch: string;
  iconSignOff: string;

  constructor(private oauth :OAuthService) { 
    this.iconSearch = "assets/images/icon-search.png";
    this.iconSignOff = "assets/images/icon-sign-off.png";
  }

  ngOnInit() {
    let menu = document.getElementById("menuMain");
    let iconMenu = document.getElementById("iconMenu");
    let wrap = document.getElementById("wrapMain");
    let contentMenu = document.getElementById("menuContent");

    if (screen.width >= 1050){
      iconMenu.addEventListener("click", openMenu, false);
    }else{
      iconMenu.addEventListener("click", openMenuMobile, false);
    }

    function openMenu(){
        menu.classList.toggle("activeMenu");
        wrap.classList.toggle("activeWrap");
        contentMenu.classList.toggle("activeContentMenu");
    }

    function openMenuMobile() {
      menu.classList.toggle("activeMenuMobile");
    }
  }

  public signOut(){
    this.oauth.logOut();
  }

}

