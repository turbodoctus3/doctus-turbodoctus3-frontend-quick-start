import { RolEnum } from "./shared/enums/rol-enum.model";

var navigationIn = [];

const navigationEx = [
    {
      name: 'Home',
      url: '/home',
      icon: 'fa fa-users',
      roles: [RolEnum.administrador],
      children: [
        {
          name: 'Home child',
          url: '/home',
          icon: 'fa fa-asterisk',
          roles: [RolEnum.administrador]
        }
      ]
    }
];

navigationIn = navigationEx;

export const navigation = navigationIn;

