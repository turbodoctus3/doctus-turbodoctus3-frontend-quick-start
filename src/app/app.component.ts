import { Component, OnInit } from '@angular/core';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from './config/auth-config';
import { Router } from '@angular/router';

@Component({
  selector: 'body',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent  {

  constructor(private oauthService: OAuthService,
    private router: Router) {
    //la implementacion de la parte de autenticacion esta inicialmente comentada
    //El desarrolador decide de que forma va desarrollar el inicio de session en la aplicacion
    //this.ConfigureImplicitFlowAuthentication();
}


private ConfigureImplicitFlowAuthentication() {
  
  this.oauthService.configure(authConfig);
  this.oauthService.tokenValidationHandler = new JwksValidationHandler();

  this.oauthService.tryLogin().then(() => {
    if (!this.oauthService.hasValidAccessToken() || !this.oauthService.hasValidIdToken()) {
        this.oauthService.initImplicitFlow();
    } else {
        if (!this.router.getCurrentNavigation()) {
            this.router.navigate(['/home']);
        }
    }
  });
 }

}
