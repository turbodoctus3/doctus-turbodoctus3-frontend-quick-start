import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './shared/containers/layout/layout.component';
import { AuthGuardService } from './shared/services/auth/auth-guard.service';
import { ErrorPageComponent } from './views/error-page/error-page.component';
import { HomeComponent } from './views/home/home.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
    // canActivate:[AuthGuardService],
  },
  {
    path: '',
    component: LayoutComponent,
    data: {
      title: 'Home'
    },
    // canActivate:[AuthGuardService],
    children: [
      /* Patn ejemplo*/
      // {
      //   path: 'account',
      //   loadChildren: './views/accounts/account/account.module#AccountModule'
      // },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: '404',
        component: ErrorPageComponent,
      },
      {
       path: '**',
       redirectTo: '404'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }