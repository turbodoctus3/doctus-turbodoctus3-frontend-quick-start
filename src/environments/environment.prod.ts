export const environment = {
  production: false,
  apiGatewayURL:'http://localhost:50330/',
  auth: {
      issuer: 'https://localhost:44374',
      loginUrl: 'https://localhost:44374/connect/authorize',
      logoutUrl: 'https://localhost:44374/connect/endsession',
      redirectUri: 'https://localhost:4200',
      key: {
          kty: "RSA",
          use: "sig",
          kid: "dcfca98b0c0a65c20b37f3cede9a490f",
          e: "AQAB",
          n: "nK8J_HP-YSrip08lrzcF1P0KVal95WS9OROJcyNAyEV8O71UxS1t0qNvyWu2Y3u-PkvlFy5GhHj5rIxQgDu9luiO-JP73kLL8Rj0ytVCTo4ICOBiDEOtLP8RxsoFVNvwwWSp8wssIhjXsYo0RtBZGlGFwhiNy6_NGmqL2PpXX3CcvfwdFWm9Ik7zjivjFYYUWUP180eiEKXAOwfJ0nEdozE32Z44h9qORsU0UxJwTLUjM4X2svgTgStG8Lb5uZcbLcyR-vMnHLrCGttjLbdAn1jBU9lgXfOucJFHvQuCPhTqd6FZ8Goc-WzFMbL_lzTchb6kJBf6wEUsGCS7m9_NBQ",
          alg: "RS256"
      }
  }
};
